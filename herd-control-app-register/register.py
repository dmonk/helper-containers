import requests
import os
import time
import sys

registered = False
for i in range(100):
    print("Registering board - Attempt {} of 100".format(i))
    r = requests.post(
        "http://{}/api/{}/boards/".format(
            os.getenv("SHEP_SERVICE"),
            os.getenv("API_VERSION")
        ), 
        data = {
            "ip": os.getenv("IP_ADDRESS"),
            "type": os.getenv("BOARD_TYPE")
        }
    )
    if r.status_code == 200:
        print("Registration successful.")
        break
    time.sleep(1)
else:
    sys.exit(1)

from minio import Minio
import datetime
import os

def main():
    bucket = os.getenv("MINIO_BUCKET")
    client = Minio(
        os.getenv("MINIO_HOSTNAME"),
        os.getenv("ACCESS_KEY"),
        os.getenv("SECRET_KEY"),
        secure=True if os.getenv("SECURE") == "true" else False
    )
    print("Checking for old artifacts...")
    counter = 0
    last_week = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=int(os.getenv("RETENTION_DAYS")))
    for obj in client.list_objects(bucket):
        if obj.last_modified < last_week:
            counter += 1
            client.remove_object(bucket, obj.object_name)
    print("Successfully removed {} artifacts.".format(counter))


if __name__ == "__main__":
    main()
